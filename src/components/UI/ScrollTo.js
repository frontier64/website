import React, { Component } from 'react';

export default class ScrollTo extends Component {
  myRef = React.createRef();

  handleScrollToElement() {
    if (this.props.index === this.props.scrollToIndex) {
      window.scrollTo(0, this.myRef.current.offsetTop);
    }
  }

  componentDidMount() {
    this.handleScrollToElement();
  }

  render() {
    return <div ref={this.myRef}>{this.props.children}</div>;
  }
}
