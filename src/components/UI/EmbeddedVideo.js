import React from 'react';

const containerStyle = {
  position: 'relative',
  height: '0',
  overflow: 'hidden',
  maxWidth: '100%',
  paddingBottom: '50vh',
};

const iframeStyle = {
  position: 'absolute',
  top: '0',
  left: '0',
  width: '100%',
  height: '100%',
};

const embeddedVideo = props => (
  <div style={containerStyle}>
    <iframe
      title={props.title}
      style={iframeStyle}
      frameBorder="0"
      align="middle"
      src={props.videoSrc}
    />
  </div>
);

export default embeddedVideo;
