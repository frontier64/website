import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import ListGroup from 'react-bootstrap/ListGroup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import StaticCard from '../../UI/StaticCard';

const carouselItems = Array.from(Array(9).keys()).map(x => {
  return (
    <Carousel.Item key={x}>
      <img
        src={`https://vidyascape.org/files/img/carousel/${x}.png`}
        key={`${x}.png`}
        alt={`${x}.png`}
      />
    </Carousel.Item>
  );
});

const listGroupClasses = 'p-1';

function listGroupHeader(text) {
  return (
    <h5>
      <strong>{text}</strong>
    </h5>
  );
}

const about = props => (
  <StaticCard
    title={<strong>The Most Complete 2007 Recreation</strong>}
    headerClasses={'text-center'}
    bodyClasses={'text-center'}
  >
    <Row>
      <Col xs={12} sm={12} md={12} lg={6}>
        <p>
          <strong>Vidyascape</strong> (or /v/scape for short) is a fully
          featured remake.
        </p>

        <ListGroup>
          <ListGroup.Item className={listGroupClasses}>
            {listGroupHeader('2.25x experience rate')}
            A little less grind allows you to progress slightly faster, but
            nothing here is free.
            <br />
            <br />
          </ListGroup.Item>

          <ListGroup.Item className={listGroupClasses}>
            {listGroupHeader('All the content you remember')}
            Our development team focuses on implementing content as much as they
            do any other feature. With nearly 100 quests and plenty of minigames
            you'll feel the nostalgia like no other server.
            <br />
            <br />
          </ListGroup.Item>

          <ListGroup.Item className={listGroupClasses}>
            {listGroupHeader('Active development')}
            Vidyascape launched in March of 2014 and has had nearly perfect
            uptime since. With a history as long as ours, you won't see us going
            anywhere anytime soon.
            <br />
            <br />
          </ListGroup.Item>

          <ListGroup.Item className={listGroupClasses}>
            {listGroupHeader('No donation benefits')}
            It's entirely free forever. Only your in game wealth matters,
            nothing real world earns you anything.
            <br />
            <br />
          </ListGroup.Item>
        </ListGroup>
      </Col>

      <Col xs={12} sm={12} md={12} lg={6}>
        <Carousel indicators={false}>{carouselItems}</Carousel>
      </Col>
    </Row>
  </StaticCard>
);

export default about;
