import React from 'react';
import Pagination from 'react-bootstrap/Pagination';

const linkBgStyle = { width: '40px' };

function generatePages(pages, page, endPage, click) {
  return pages(page, endPage).map(x => (
    <Pagination.Item
      key={'pgnav' + x}
      style={linkBgStyle}
      active={page === x}
      onClick={() => click(x)}
    >
      <div style={linkBgStyle}>
        {x > 99 ? <p style={{ height: '2px', marginLeft: '-5px' }}>{x}</p> : x}
      </div>
    </Pagination.Item>
  ));
}

const pageNavigation = props => (
  <Pagination className={'justify-content-center'}>
    <Pagination.First
      style={{ borderRadius: '4px 0 0 4px' }}
      disabled={props.page <= 1}
      onClick={() => props.click(1)}
    />
    <Pagination.Prev
      style={{ borderRadius: '4px 0 0 4px' }}
      disabled={props.page <= 1}
      onClick={() => props.click(props.page > 1 ? props.page - 1 : props.page)}
    />

    {generatePages(props.pages, props.page, props.endPage, props.click)}

    <Pagination.Next
      style={{ borderRadius: '4px 0 0 4px' }}
      disabled={props.page >= props.endPage}
      onClick={() =>
        props.click(props.page < props.endPage ? props.page + 1 : props.page)
      }
    />
    <Pagination.Last
      style={{ borderRadius: '4px 0 0 4px' }}
      disabled={props.page >= props.endPage}
      onClick={() => props.click(props.endPage)}
    />
  </Pagination>
);

export default pageNavigation;
