import React from 'react';

const skillImage = props => (
  <img
    width="15px"
    height="15px"
    src={`${require(`../../assets/images/skills/${props.skill}.gif`)}`}
    alt={props.skill}
  />
);

export default skillImage;
