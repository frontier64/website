import React from 'react';
import Button from 'react-bootstrap/Button';

const skillIcon = props => (
  <Button
    onClick={() => props.clickEvent(props.skill)}
    variant="secondary"
    active={props.active}
    className={'p-2'}
  >
    <img
      width="18px"
      height="18px"
      src={`${require(`../../assets/images/skills/${props.skill}.gif`)}`}
      alt={props.skill}
    />
  </Button>
);

export default skillIcon;
