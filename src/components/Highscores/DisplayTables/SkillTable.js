import React from 'react';

import {
  formatNumber,
  getIronmanIcon,
  formatPlayerName,
} from '../../../utils/highscores';

import ResponsiveTable from '../../UI/ResponsiveTable';

function generateHeader() {
  return (
    <tr>
      <th className={'text-right'} style={{ width: '70px' }}>
        Rank
      </th>
      <th className={'text-center'}>Username</th>
      <th className={'text-center'}>Level</th>
      <th className={'text-center'}>Experience</th>
    </tr>
  );
}

function generateBody(click, skill, page, data) {
  return data.map((p, k) => (
    <tr
      onMouseUp={event => click(event, p.username)}
      key={p.username}
      id={p.username}
      style={{ cursor: 'pointer' }}
    >
      <td className={'text-right'}>{formatNumber(p[skill + '_rank'])}</td>
      <td className={'text-center'}>
        {getIronmanIcon(p.ironman)} {formatPlayerName(p.username)}
      </td>
      <td className={'text-center'}>{formatNumber(p[skill + '_lvl'])}</td>
      <td className={'text-center'}>{formatNumber(p[skill + '_xp'])}</td>
    </tr>
  ));
}

const skillTable = props => (
  <ResponsiveTable
    header={generateHeader()}
    body={generateBody(props.clickEvent, props.skill, props.page, props.data)}
  />
);

export default skillTable;
