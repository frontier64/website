import React from 'react';

import Aux from '../../../hoc/Auxiliary';
import SkillImage from '../../../components/Highscores/SkillImage';
import ResponsiveTable from '../../UI/ResponsiveTable';

import {
  getValidSkills,
  formatNumber,
  formatSectionName,
  formatPlayerName,
  getValidStats,
} from '../../../utils/highscores';

function compare(props, s) {
  let dataOne = 0;
  let dataTwo = 0;
  if (getValidStats().includes(s)) {
    dataOne = props.data[s];
    dataTwo = props.dataTwo[s];
  } else if (props.compareFilter === 'experience') {
    dataOne = props.data[s + '_xp'];
    dataTwo = props.dataTwo[s + '_xp'];
  } else if (props.compareFilter === 'level') {
    dataOne = props.data[s + '_lvl'];
    dataTwo = props.dataTwo[s + '_lvl'];
  }
  if (dataOne > dataTwo) {
    return 1;
  } else if (dataOne === dataTwo) {
    return 2;
  } else if (dataOne < dataTwo) {
    return 3;
  }
}

function generateStyle(props, s, i) {
  const c = compare(props, s);
  if (i === 1) {
    return c === 1
      ? { backgroundColor: '#00bc8c' }
      : c === 2
      ? { backgroundColor: '#f39c12' }
      : { backgroundColor: '#e74c3c' };
  } else {
    return c === 1
      ? { backgroundColor: '#e74c3c' }
      : c === 2
      ? { backgroundColor: '#f39c12' }
      : { backgroundColor: '#00bc8c' };
  }
}

/*Skills Compare Table*/
function generatePlayerNames(props) {
  return (
    <tr>
      <th style={{ borderRightColor: '#375a7f' }} />
      <th
        colSpan="3"
        style={{
          cursor: 'pointer',
          borderLeftColor: '#375a7f',
          borderRightColor: '#375a7f',
        }}
        className={'text-center'}
        onClick={() => props.clickEventPlayer(props.data['username'])}
      >
        {formatPlayerName(props.data['username'])}
      </th>
      <th style={{ borderLeftColor: '#375a7f', borderRightColor: '#375a7f' }} />
      <th
        colSpan="3"
        style={{ cursor: 'pointer', borderLeftColor: '#375a7f' }}
        className={'text-center'}
        onClick={() => props.clickEventPlayer(props.dataTwo['username'])}
      >
        {formatPlayerName(props.dataTwo['username'])}
      </th>
    </tr>
  );
}

function generateSkillsHeader() {
  return (
    <tr>
      <th className={'text-center'}>Skill</th>
      <th className={'text-center'}>Rank</th>
      <th className={'text-center'}>Level</th>
      <th className={'text-center'}>Experience</th>
      <th />
      <th className={'text-center'}>Rank</th>
      <th className={'text-center'}>Level</th>
      <th className={'text-center'}>Experience</th>
    </tr>
  );
}

function generateCompareHeader(props) {
  return (
    <Aux>
      {generatePlayerNames(props)}
      {generateSkillsHeader()}
    </Aux>
  );
}

function generateSkillsBody(props) {
  return getValidSkills().map(s => (
    <tr key={s}>
      <td
        onClick={() => props.clickEvent(s)}
        style={{ cursor: 'pointer' }}
        className={'text-center'}
      >
        <SkillImage skill={s} /> {formatSectionName(s)}
      </td>
      <td style={generateStyle(props, s, 1)} className={'text-center'}>
        {formatNumber(props.data[s + '_rank'])}
      </td>
      <td style={generateStyle(props, s, 1)} className={'text-center'}>
        {formatNumber(props.data[s + '_lvl'])}
      </td>
      <td style={generateStyle(props, s, 1)} className={'text-center'}>
        {formatNumber(props.data[s + '_xp'])}
      </td>
      <td className={'text-center'}>
        {compare(props, s) === 1 ? '>' : compare(props, s) === 2 ? '=' : '<'}
      </td>
      <td style={generateStyle(props, s, 2)} className={'text-center'}>
        {formatNumber(props.dataTwo[s + '_rank'])}
      </td>
      <td style={generateStyle(props, s, 2)} className={'text-center'}>
        {formatNumber(props.dataTwo[s + '_lvl'])}
      </td>
      <td style={generateStyle(props, s, 2)} className={'text-center'}>
        {formatNumber(props.dataTwo[s + '_xp'])}
      </td>
    </tr>
  ));
}

/* Stat Compare Table */
function generateStatsHeader() {
  return (
    <tr>
      <th className={'text-center'}>Stat</th>
      <th className={'text-center'}>Rank</th>
      <th className={'text-center'}>Score</th>
      <th />
      <th className={'text-center'}>Rank</th>
      <th className={'text-center'}>Score</th>
    </tr>
  );
}

function generateCompareStatsHeader(props) {
  return generateStatsHeader();
}

function generateStatsBody(props) {
  return getValidStats().map(g => (
    <tr key={g}>
      <td
        onClick={() => props.clickEvent(g)}
        style={{ cursor: 'pointer' }}
        className={'text-center'}
      >
        <SkillImage skill={g} /> {formatSectionName(g)}
      </td>
      <td style={generateStyle(props, g, 1)} className={'text-center'}>
        {props.data[g + '_rank']}
      </td>
      <td style={generateStyle(props, g, 1)} className={'text-center'}>
        {props.data[g]}
      </td>
      <td className={'text-center'}>
        {compare(props, g) === 1 ? '>' : compare(props, g) === 2 ? '=' : '<'}
      </td>
      <td style={generateStyle(props, g, 2)} className={'text-center'}>
        {props.dataTwo[g + '_rank']}
      </td>
      <td style={generateStyle(props, g, 2)} className={'text-center'}>
        {props.dataTwo[g]}
      </td>
    </tr>
  ));
}

const playerTable = props => (
  <Aux>
    <ResponsiveTable
      header={generateCompareHeader(props)}
      body={generateSkillsBody(props)}
    />
    <ResponsiveTable
      header={generateCompareStatsHeader(props)}
      body={generateStatsBody(props)}
    />
  </Aux>
);

export default playerTable;
