import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Layout from './hoc/Layout';
import classes from './App.css';

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Layout className={classes} />
      </BrowserRouter>
    );
  }
}
