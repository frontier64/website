import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://vidyascape.org/',
});

export default instance;
