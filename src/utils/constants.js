export const vscapeFileRoot = 'https://vidyascape.org/files/';

export const DAY_IN_SECONDS = 86400;
export const WEEK_IN_SECONDS = 604800;
export const MONTH_IN_SECONDS = 2592000;
export const HIGHSCORES = 'highscores';
export const TRACKER = 'tracker';
export const PLAYER = 'player';
export const COMPARE = 'compare';
export const NO_RESP = 'no response';
export const API_TIMEOUT = 10000;
