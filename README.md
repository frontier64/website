# Development

Install dependencies

```
npm install
```

Start local server

```
npm start
```

Before opening a pull request, use...

```
npm run fix
```

This will format your code and run eslint on it. Fix any errors or warnings the linter throws. There is also a pre-commit hook that will automatically format staged files.

# Production

Build the minified and optimized version

```
npm build
```

And then throw the build directory on the webserver.
